
## Unpublished changes

## Version 0.2.0

### Changes

- Rewritten a lot of the UI using WoAB
    - Maybe the editor is broken now
- New file format version 2
- Improvements to loading times and responsiveness
- Songs are now sorted alphabetically

## Version 0.1.0

### Changes

- Initial crappy release
